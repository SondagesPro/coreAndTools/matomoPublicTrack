# matomoPublicTrack

Track public page with matomo , with custom dimension, variables and more advanced settings.

# Usage

After plugin activation, you can update settings to set basic information for matomo : url and site id.

You can set all default settings for all survey at global settings.

- *Track Survey pages* : default settings for all survey, can be found in survey setings too.
- *URL to matomo* : The url for matomo website
- *Piwik SiteId* : The site id from matomo, see [What should you put here?](https://matomo.org/faq/general/faq_19212/)
- *Rewrite Survey page URLs* : 3 method for page url in matomo
    - None : usage of real url
    - Tweaked URL : do a twicked url to show pages like a tree by survey, group, question … 
    - Admin URL : Corresponding administration page
    - Public URL : Start of survey for survey
- *Add the matomo plugin param.* : send the extra parameters to be used with [matomo LimeSurveyUserProfile plugin](https://gitlab.com/SondagesPro/External/LimeSurveyUserProfile)
- *Respect Do Not Track.* : respect (or not) do not track enable by navigator
- *Set matomo user id* : Matomo allow you to set user id, see [Matomo documentation about this setting](https://matomo.org/docs/user-id), the [Expression Manager](https://manual.limesurvey.org/Expression_Manager) is available.
- *Number of custom variables* : Allow to set the max number of available variables for survey. See [Matomo documentation about Custom Variables](https://matomo.org/docs/custom-variables/).
- *Number of custom dimensions* :  Allow to set the max number of available variables for survey. See [Matomo documentation about Custom Dimensions](https://matomo.org/docs/custom-dimensions/).
- *Custom variable label* : Each variables need a label, this label is set fo alll survey here. Expression manager can not be used.
- *Custom variable value* : Default value for each variables value set here, can be updated in each surveys, the [Expression Manager](https://manual.limesurvey.org/Expression_Manager) is available.
- *Custom dimension value (by id)* : Default value for each dimensions value set here, can be updated in each surveys, the [Expression Manager](https://manual.limesurvey.org/Expression_Manager) is available.

In each survey, on tool menu : you can access to survey specific settings. You can choose if survey is tracked via matomo or not.

## Custom variables and dimensions

You can send custom dimension and custom variables to matomo. The value can used Expression from LimeSurvey, then you can send information about current data in current survey.

The custom variables en dimension are sent as [`visit` scope](https://matomo.org/docs/real-time/#visits-log).

For string : if you leave empty value : the global default is used (you can use one space to disable usage of global seting). For each value Expression Manager is available. The result is shown as help then you can check if there are error.

After expression, the value is trimmed. If the final value is empty : the parameters are not set for matomo.

### Some example of usage

- User id by survey and token (available if survey is not anonymous) : `{SID}-{TOKEN}`
- User id by email of token (available if survey is not anonymous) : `{TOKEN:EMAIL}`
- Send the gender (set in question with title gender) in a dimensions :  `{GENDER.NAOK}`
