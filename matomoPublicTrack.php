<?php
/**
 * Plugin to add matomo script in public part
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019-2023 Denis Chenu <http://www.sondages.pro>
 * @license GPL v3
 * @version 0.4.4
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class matomoPublicTrack extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Add matomo script and img in public part.';
    protected static $name = 'matomoPublicTrack';

    /**
    * The current controller
    * @var string
    * @todo check if we can remove
    */
    public $sMatomoCustomController = 'default';

    /**
     * Array of current registered script : to register all needed part
     * @var integer[]
     */
    private $aRegisteredParams = array(
        'controller' => null,
        'sid' => null,
        'gid' => null,
        'event' => null,
    );

    /**
    * The custom parameters for the final url
    * @var array : all url params finally used
    */
    public $aMatomoCustomUrlParams = array(
        'base' => null,
        'surveyid' => null,
        'lang' => null,
        'gid' => null,
        'qid' => null,
        'event' => null,
        'action' => null,
    );

    /**
    * Matomo plugin optionnal params
    * @var array : all url params finally used
    * @todo check if we can remove
    */
    public $aMatomoPluginParams = array(
        'surveyid' => null,
        'srid' => null,
        'token' => null,
    );

    /* @var boolean : track a new visit */
    private $setNewVisit = false;

    /** The settings **/
    protected $settings = array(
        'trackSurveyPages' => array(
            'type'=>'select',
            'options'=>array(
                0=>'No',
                1=>'Yes'
            ),
            'label'=>'Track Survey pages by default.',
            'default'=> 1
        ),
        'matomoURL'=>array(
            'type'=>'string',
            'label'=>'URL to matomo',
            'help' => "Can be relative or absolute",
            'default'=>'/piwik/'
        ),
        'siteID'=>array(
            'type'=>'int',
            'label'=>"Piwik SiteId",
            'help' => "<a href='https://matomo.org/faq/general/faq_19212/' target='_new'>What should you put here?</a>",
            'htmlOptions'=>array(
                'min' => 0,
            ),
            'default'=>1
        ),
        'crossDomainLinking'=>array(
            'type'=>'text',
            'label'=>"List of url for cross domain linking (one domain by lines)",
            'help' => "<a href='https://matomo.org/faq/how-to/faq_23654/' target='_new'>How do I accurately measure the same visitor across multiple domain names (cross domain linking)?</a>",
            'default'=>""
        ),
        'rewriteURLs'=>array(
            'type'=>'select',
            'options'=>array(
                'tweak'=>'Yes, to a tweaked URL',
                'admin'=>'Yes, to corresponding admin page',
                'public'=>'Yes, to public page with more information (start of survey).',
            ),
            'htmlOptions'=>array(
                'empty' => 'None, usage of real url',
            ),
            'help' => 'Tweaked show page with group and action as sub-directory. Admin to the survey administration page, and public add extra parameters for current group or action.',
            'label'=>'Rewrite Survey page URLs to store more information in matomo.',
            'default'=>'tweak'
        ),
        'limesurveyVisitPlugin' => array(
            'type'=>'select',
            'options'=>array(
                0=>'No',
                1=>'Yes'
            ),
            'label'=>'Add the matomo plugin param.',
            'default'=> 1
        ),
        //~ 'usePHPApi' => array(
            //~ 'type'=>'select',
            //~ 'options'=>array(
                //~ 0=>'No',
                //~ 1=>'Yes'
            //~ ),
            //~ 'label'=>'Use php API (and not the javascript solution).',
            //~ 'help' => 'See <a href="https://github.com/matomo-org/matomo-php-tracker" target="_blank">matomo-php-tracker repository</a> for more information',
            //~ 'default'=> 0
        //~ ),
        'respectDNT' => array(
            'type'=>'select',
            'options'=>array(
                0=>'No',
                1=>'Yes'
            ),
            'label'=>'Respect Do Not Track.',
            'default'=> 1
        ),
        'matomoUserId' => array(
            'type'=>'string',
            'label'=>'Set matomo user id',
            'help' => "If you set a value here : LimeSurvey use it to construct <a href='https://matomo.org/docs/user-id' target='_blank'>the matomo user id</a>. You can use expression manager, it final user is is empty : no user id is set.",
            'default'=>''
        ),
        'customVariablesNumber' => array(
            'type'=>'int',
            'htmlOptions'=>array(
                'min' => 0,
            ),
            'label'=>'Number of custom variables',
            'help'=>"See <a href='https://matomo.org/docs/custom-variables/' target='_new'>Custom Variables on matomo website</a>, set the max number of available variables. Label is fixed, value can be updated in survey.",
            'default'=>0
        ),
        'customDimensionsNumber' => array(
            'type'=>'int',
            'htmlOptions'=>array(
                'min' => 0,
            ),
            'label'=>'Number of custom dimensions',
            'help'=>"See <a href='https://matomo.org/docs/custom-dimensions/' target='_new'>Custom Dimensions on matomo website</a>, set the max number of available dimensions.",
            'default'=>0
        ),
        'customVariables' => array(
            'type'=>'info',
            'content'=>'',
        ),
        'customDimensions' => array(
            'type'=>'info',
            'content'=>'',
        ),
    );

    /* @inhetitdoc */
    public $allowedPublicMethods = [
        'actionSaveSettings',
        'actionSettings',
    ];

    /** Register to events **/
    public function init()
    {
        $this->subscribe('beforeSurveyPage');
        $this->subscribe('beforeControllerAction');
        //~ $this->subscribe('beforeCloseHtml');
        $this->subscribe('getPluginTwigPath', 'addMatomoScript');
        $this->subscribe('beforeTwigRenderTemplate', 'addMatomoScript');
        $this->subscribe('beforeCloseHtml', 'addMatomoScript');

        /* To add specific params */
        $this->subscribe('beforeQuestionRender');
        $this->subscribe('afterSurveyComplete');

        /* Params */
        $this->subscribe('beforeToolsMenuRender');
    }

    /** @inheritdoc **/
    public function beforeToolsMenuRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $event = $this->getEvent();
        $surveyId = $event->get('surveyId');
        $aMenuItem = array(
            'label' => $this->gT('Matomo'),
            'iconClass' => 'fa fa-binoculars',
            'href' => Yii::app()->createUrl(
                'admin/pluginhelper',
                array(
                    'sa' => 'sidebody',
                    'plugin' => get_class($this),
                    'method' => 'actionSettings',
                    'surveyId' => $surveyId
                )
            ),
        );
        if (class_exists("\LimeSurvey\Menu\MenuItem")) {
            $menuItem = new \LimeSurvey\Menu\MenuItem($aMenuItem);
        } else {
            $menuItem = new \ls\menu\MenuItem($aMenuItem);
        }
        $event->append('menuItems', array($menuItem));
    }

    /**
     * Main function for settings
     * @param int $surveyId Survey id
     *
     * @return string
     */
    public function actionSettings($surveyId)
    {
        $oSurvey=Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, $this->translate("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        if (App()->getRequest()->getPost('save'.get_class($this))) {
            $postedValues = App()->getRequest()->getPost(get_class($this).'settings');
            if (App()->getRequest()->getPost('save'.get_class($this)=='redirect')) {
                Yii::app()->getController()->redirect(Yii::app()->getController()->createUrl('admin/survey', array('sa'=>'view', 'surveyid'=>$surveyId)));
            }
        }

        $aData['pluginClass']=get_class($this);
        $aData['surveyId']=$surveyId;
        $aData['lang'] = array(
            'Matomo settings for this survey' => $this->gT('Matomo settings for this survey'),
        );
        $surveyViewController = (version_compare(App()->getConfig('versionnumber'), '4', "<")) ? 'admin/survey/sa/view' : 'surveyAdministration/view';
        $aData['form'] = array(
            'action' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSaveSettings','surveyid' => $surveyId)),
            'reset' => Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyid' => $surveyId)),
            'close' => Yii::app()->createUrl($surveyViewController, array('surveyid' => $surveyId)),
        );

        $aSettings=array();
        $userIDHelp = "";
        $matomoUserId = $this->get('matomoUserId', 'Survey', $surveyId);
        if (empty($matomoUserId)) {
            $matomoUserId = $this->get('matomoUserId', null, null, '');
        }
        if ($matomoUserId) {
            $userIDHelp = sprintf($this->gT("Actual value: %s"), $this->_getHtmlExpression($matomoUserId, $surveyId));
        }
        $aBaseSettings = array(
            'trackSurveyPages' => array(
                'type' => 'select',
                'options' => array(
                    'Y'=> gT('Yes'),
                    'N'=> gT('No'),
                ),
                'label' => $this->gT("Track this survey ."),
                'htmlOptions' => array(
                    'empty' => CHtml::encode(sprintf($this->gT("Use default (%s)"), ($this->get('trackSurveyPages', null, null, 1) ? gT('Yes') : gT('No')))),
                ),
                'help' => "",
                'current'=>$this->get('trackSurveyPages', 'Survey', $surveyId),
            ),
            'matomoUserId'=>array(
                'type'=>'string',
                'label'=>$this->gT('Set matomo user id'),
                'help' => $userIDHelp,
                'current'=>$this->get('matomoUserId', 'Survey', $surveyId),
            ),
        );
        $aSettings[$this->gT("Basic setting")] = $aBaseSettings;

        $aCustomVariables = array();
        $customVariablesNumber =  $this->get('customVariablesNumber', null, null, 0);
        if ($customVariablesNumber > 0) {
            $customBaseVariables = $this->get('customVariables', null, null, "[]");
            $customBaseVariables = json_decode($customBaseVariables, 1);
            $customVariables = $this->get('customVariables', 'Survey', $surveyId, array());
            //~ $customVariablesNumber = isset($pluginSettings['customVariables']) ? intval($pluginSettings['customVariablesNumber']['current']) : 0;
            for ($variableCount = 1; $variableCount <= $customVariablesNumber; $variableCount++) {
                $label = isset($customBaseVariables[$variableCount]['label']) ? $customBaseVariables[$variableCount]['label'] : '';
                if ($label) {
                    $current = isset($customVariables[$variableCount]) ? $customVariables[$variableCount] : null;
                    $final = $current;
                    if (empty($final)) {
                        $final = isset($customBaseVariables[$variableCount]['value']) ? $customBaseVariables[$variableCount]['value'] : null;
                    }
                    $help = "";
                    if ($final) {
                        $help = sprintf($this->gT("Actual value: %s"), $this->_getHtmlExpression($final, $surveyId));
                    }
                    $aCustomVariables["customVariables[$variableCount]"] = array(
                        'type'=>'string',
                        'label'=>sprintf($this->gT("%s (Custom variable)"), $label),
                        'current'=>$current,
                        'help' => $help,
                    );
                }
            }
            $aSettings[$this->gT("Custom variables")] = $aCustomVariables;
        }

        $aCustomDimensions = array();
        $customDimensionNumber =  $this->get('customDimensionsNumber', null, null, 0);
        if ($customDimensionNumber > 0) {
            $customBaseDimensions = $this->get('customDimensions', null, null, "[]");
            $customBaseDimensions = json_decode($customBaseDimensions, 1);
            $customDimensions = $this->get('customDimensions', 'Survey', $surveyId, array());
            for ($dimensionsCount = 1; $dimensionsCount <= $customDimensionNumber; $dimensionsCount++) {
                $current = isset($customDimensions[$dimensionsCount]) ? $customDimensions[$dimensionsCount] : null;
                $final = $current;
                if (empty($final)) {
                    $final = isset($customBaseDimensions[$dimensionsCount]) ? $customBaseDimensions[$dimensionsCount] : null;
                }
                $help = "";
                if ($final) {
                    $help = sprintf($this->gT("Actual value: %s"), $this->_getHtmlExpression($final, $surveyId));
                }
                $aCustomDimensions["customDimensions[$dimensionsCount]"] = array(
                    'type'=>'string',
                    'label'=>sprintf($this->gT("Dimension #%s"), $dimensionsCount),
                    'current'=>$current,
                    'help' => $help,
                );
            }
            $aSettings[$this->gT("Custom dimensions")] = $aCustomDimensions;
        }

        $aData['aSettings'] = $aSettings;
        $content = $this->renderPartial('settings', $aData, true);
        return $content;
    }

    /**
     * The main function
     * @param int $surveyId Survey id
     * @return void (redirect)
     */
    public function actionSaveSettings($surveyId)
    {
        if (empty(App()->getRequest()->getPost('save'.get_class($this)))) {
            throw new CHttpException(400);
        }
        $oSurvey=Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
            throw new CHttpException(404, gT("This survey does not seem to exist."));
        }
        if (!Permission::model()->hasSurveyPermission($surveyId, 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        /* The settings save */
        \PluginSetting::model()->deleteAll("plugin_id = :pluginid AND model = :model AND model_id = :sid", array(":pluginid"=>$this->id,":model"=>'Survey',':sid'=>$surveyId));
        /* Base settings */
        $this->set('trackSurveyPages', App()->getRequest()->getPost('matomoPublicTrack', ""), 'Survey', $surveyId);
        $this->set('matomoUserId', App()->getRequest()->getPost('matomoUserId', ""), 'Survey', $surveyId);
        /* Custom variables */
        $customVariables = (array) App()->getRequest()->getPost('customVariables', null);
        $this->set('customVariables', $customVariables, 'Survey', $surveyId);
        /* Custom dimensions */
        $customDimensions = (array) App()->getRequest()->getPost('customDimensions', null);
        $this->set('customDimensions', $customDimensions, 'Survey', $surveyId);
        if (App()->getRequest()->getPost('save'.get_class($this)) == 'redirect') {
            $surveyViewController = (version_compare(App()->getConfig('versionnumber'), '4', "<")) ? 'admin/survey/sa/view' : 'surveyAdministration/view';
            $redirectUrl = App()->createUrl($surveyViewController, array('surveyid' => $surveyId));
            Yii::app()->getRequest()->redirect($redirectUrl);
        }
        $redirectUrl = Yii::app()->createUrl('admin/pluginhelper/sa/sidebody', array('plugin' => get_class($this),'method' => 'actionSettings','surveyid' => $surveyId));
        Yii::app()->getRequest()->redirect($redirectUrl, true, 303);
    }
    /**
     * @see parent:getPluginSettings
     */
    public function getPluginSettings($getValues = true)
    {
        if(!Permission::model()->hasGlobalPermission('settings','read')) {
            throw new CHttpException(403);
        }
        $pluginSettings= parent::getPluginSettings($getValues);
        if ($getValues) {
            $customVariables = 0;
            $customVariablesNumber = isset($pluginSettings['customVariablesNumber']['current']) ? intval($pluginSettings['customVariablesNumber']['current']) : 0;
            $customVariables = isset($pluginSettings['customVariables']['current']) ? @json_decode($pluginSettings['customVariables']['current'], 1) : null;
            //~ $customVariablesNumber = isset($pluginSettings['customVariables']) ? intval($pluginSettings['customVariablesNumber']['current']) : 0;
            for ($variableCount = 1; $variableCount <= $customVariablesNumber; $variableCount++) {
                $current = array(
                    'label' => isset($customVariables[$variableCount]['label']) ? $customVariables[$variableCount]['label'] : '',
                    'value'=> isset($customVariables[$variableCount]['value']) ? $customVariables[$variableCount]['value'] : '',
                );
                $pluginSettings["customVariables[{$variableCount}][label]"] = array(
                    'type'=>'string',
                    'label'=>sprintf($this->gT("Custom variable label %s"), $variableCount),
                    'current'=>$current['label'],
                    'prefix'=>'customVariables',
                );
                $pluginSettings["customVariables[{$variableCount}][value]"] = array(
                    'type'=>'string',
                    'label'=>sprintf($this->gT("Custom variable %s"), $variableCount),
                    'current'=>$current['value'],
                    'help' => $variableCount ==1 ? $this->gT("You can use expression manager, remind to add { and }. For non admin user : invalid ") : "",
                );
            }
            $customDimensions = 0;
            $customDimensionsNumber = isset($pluginSettings['customDimensionsNumber']['current']) ? intval($pluginSettings['customDimensionsNumber']['current']) : 0;
            $customDimensions = isset($pluginSettings['customDimensions']['current']) ? @json_decode($pluginSettings['customDimensions']['current'], 1) : null;
            //~ $customDimensionsNumber = isset($pluginSettings['customDimensions']) ? intval($pluginSettings['customDimensionsNumber']['current']) : 0;
            for ($dimensionsCount = 1; $dimensionsCount <= $customDimensionsNumber; $dimensionsCount++) {
                $current = array(
                    'value'=> isset($customDimensions[$dimensionsCount]) ? $customDimensions[$dimensionsCount] : '',
                );
                $pluginSettings["customDimensions[{$dimensionsCount}]"] = array(
                    'type'=>'string',
                    'label'=>sprintf($this->gT("Custom dimension id %s value"), $dimensionsCount),
                    'current'=>$current['value'],
                    'help' => $dimensionsCount ==1 ? $this->gT("You can use expression manager, remind to add { and }") : "",
                );
            }
        }
        unset($pluginSettings['customVariables']);
        unset($pluginSettings['customDimensions']);
        return $pluginSettings;
    }

    /**
    * set actual url when activate
    * @see parent::saveSettings()
    */
    public function saveSettings($settings)
    {
        if(!Permission::model()->hasGlobalPermission('settings','update')) {
            throw new CHttpException(403);
        }
        if (!empty($settings['customVariablesNumber'])) {
            $customVariables = Yii::app()->getRequest()->getPost('customVariables');
            $settings['customVariables'] = json_encode($customVariables);
        }
        if (!empty($settings['customDimensionsNumber'])) {
            $customDimensions = Yii::app()->getRequest()->getPost('customDimensions');
            $settings['customDimensions'] = json_encode($customDimensions);
        }
        if (!empty($settings['crossDomainLinking'])) {
            $crossDomainLinking = Yii::app()->getRequest()->getPost('crossDomainLinking');
            $aCrossDomainLinking = $this->fixDomainList($crossDomainLinking);
            if(!empty($aCrossDomainLinking)) {
                $settings['crossDomainLinking'] = implode("\n",$aCrossDomainLinking);
            } else {
                $settings['crossDomainLinking'] = "";
            }
        }
        parent::saveSettings($settings);
    }

    /** Add the setting specific to survey **/
    public function beforeSurveySettings()
    {
    }

    /* Basic params */
    public function beforeSurveyPage()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->aMatomoCustomUrlParams['base'] = 'survey';
        $this->aMatomoCustomUrlParams['surveyid'] = $this->getEvent()->get('surveyId');
        $this->aMatomoCustomUrlParams['lang'] = Yii::app()->getLanguage();

        $this->aMatomoPluginParams['surveyid'] = $this->getEvent()->get('surveyId');
        $surveyId = $this->getEvent()->get('surveyId');
        if (in_array(Yii::app()->request->getParam('action'), array('previewgroup', 'previewquestion'))) {
            $this->aMatomoCustomUrlParams['event'] = Yii::app()->request->getParam('action');
        }
        if (Yii::app()->request->getParam('loadall') == "reload") {
            $this->aMatomoCustomUrlParams['event'] = Yii::app()->request->getParam('loadall');
        }
        if (Yii::app()->request->getParam('clearall') == "clearall") {
            $this->aMatomoCustomUrlParams['event'] = Yii::app()->request->getParam('clearall');
        }
        if (Yii::app()->request->getParam('saveall') == "saveall") {
            $this->aMatomoCustomUrlParams['event'] = Yii::app()->request->getParam('saveall');
        }
        /**
         * TODO :
         * - event welcome page
         */
    }

    /* Question params */
    public function beforeQuestionRender()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oSurvey = Survey::model()->findByPk($this->getEvent()->get('surveyId'));
        if ($oSurvey->format == "A") {
            $this->aMatomoCustomUrlParams['event'] = 'surveypage';
        }
        $this->aMatomoCustomUrlParams['gid'] = $this->getEvent()->get('gid');
        if ($oSurvey->format == "S") {
            $this->aMatomoCustomUrlParams['qid'] = $this->getEvent()->get('qid');
        }
        $this->unsubscribe('beforeQuestionRender');
    }

    /* Event params */
    public function afterSurveyComplete()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->aMatomoCustomUrlParams['event'] = 'complete';
    }

    /* Event params */
    public function afterSurveyQuota()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $this->aMatomoCustomUrlParams['event'] = 'quota';
        // TODO : add quota id
    }

    /* To have controller */
    public function beforeControllerAction()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $adminControllers = [
            'admin',
            'assessment',
            'failedemail',
            'homepagesettings',
            'limereplacementfields',
            'questiongroupsadministration',
            'quicktranslation',
            'surveyadministration',
            'surveypermissions',
            'surveysgroupspermission',
            'themeoptions',
            'usergroup',
            'usermanagement',
            'userrole',
            'usermanagementcontroller'
        ];
        if (in_array(strtolower($this->getEvent()->get('controller')), $adminControllers)) {
            return;
        }
        // plugins ?
        $this->sMatomoController = $this->getEvent()->get('controller');
    }

    /* Add the script and the image */
    public function addMatomoScript()
    {
        $this->unsubscribe('getPluginTwigPath');
        $surveyId = $this->aMatomoPluginParams['surveyid'];
        $this->aRegisteredParams['controller'] = $this->sMatomoCustomController;
        $this->aRegisteredParams['sid'] = $this->aMatomoPluginParams['surveyid'];
        $this->aRegisteredParams['gid'] = $this->aMatomoCustomUrlParams['gid'];
        $this->aRegisteredParams['event'] = $this->aMatomoCustomUrlParams['event'];
        $trackSurveyPages = $this->_getCurrentSetting('trackSurveyPages', $surveyId);
        if (!$trackSurveyPages) {
            App()->getClientScript()->registerScript('matomoScriptInit', '', CClientScript::POS_BEGIN);
            return;
        }
        if ($this->get('respectDNT')) {
            $bDoNotTrackEnable = false;
            if (array_key_exists('HTTP_DNT', $_SERVER) && (1 === (int) $_SERVER['HTTP_DNT'])) {
                $bDoNotTrackEnable = true;
            }
            if ($bDoNotTrackEnable) {
                App()->getClientScript()->registerScript('matomoScriptInit', '', CClientScript::POS_BEGIN);
                return;
            }
        }

        $matomoURL = rtrim($this->_getCurrentSetting('matomoURL', $surveyId),"/") . "/";
        $siteID = intval($this->_getCurrentSetting('siteID', $surveyId));
        $matomoScriptInit = "var _paq = window._paq || [];";
        App()->getClientScript()->registerScript('matomoScriptInit', $matomoScriptInit, CClientScript::POS_BEGIN);
        $this->_setCrossDomainLinking();
        $this->_setCustomURL($surveyId);
        $this->_setCustomVariable($surveyId);
        $this->_setCustomDimension($surveyId);
        $this->_setCustomUserId($surveyId);
        $this->_setLimesurveyVisitPlugin($surveyId);
        $matomoScriptEnd = "";
        if ($this->setNewVisit) {
            $matomoScriptEnd .= "_paq.push(['appendToTrackingUrl', 'new_visit=1']);\n";
            //~ $matomoScriptEnd .= "_paq.push(['deleteCookies']);\n";
        }
        $matomoScriptEnd .= "_paq.push(['trackPageView']);\n"
             . "_paq.push(['enableLinkTracking']);\n"
             . "(function() {\n"
             . "  var u=\"{$matomoURL}\";\n"
             . "  _paq.push(['setTrackerUrl', u+'matomo.php']);\n"
             . "  _paq.push(['setSiteId', '{$siteID}']);\n"
             . "  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];\n"
             . "  g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);\n"
             . "})();\n";
        App()->getClientScript()->registerScript('matomoScriptEnd', $matomoScriptEnd, CClientScript::POS_BEGIN);
    }

    /**
     * Add the setCustomVariable to js
     * @param integer $surveyId
     * @return $void
     */
    private function _setCustomVariable($surveyId)
    {
        $aMatomoCustomVariables = array();
        $customVariablesNumber =  $this->get('customVariablesNumber', null, null, 0);
        if ($customVariablesNumber > 0) {
            $aSurveyCustomVariables = $this->get("customVariables", 'survey', $surveyId);
            $aGlobalCustomVariables = json_decode($this->get("customVariables", null, null), 1);
            for ($variablesCount = 1; $variablesCount <= $customVariablesNumber; $variablesCount++) {
                if (!empty($aGlobalCustomVariables[$variablesCount]['label'])) {
                    $label = $aGlobalCustomVariables[$variablesCount]['label'];
                    $currentVariableValue = !empty($aSurveyCustomVariables[$variablesCount]) ? $aSurveyCustomVariables[$variablesCount] : null;
                    if (empty($currentVariableValue)) {
                        $currentVariableValue = !empty($aGlobalCustomVariables[$variablesCount]['value']) ? $aGlobalCustomVariables[$variablesCount]['value'] : null;
                    }
                    if (!empty($currentVariableValue)) {
                        $currentVariableValue = $this->ProcessStepString($currentVariableValue);// force static
                        /* @todo : check error */
                        if ($currentVariableValue) {
                            
                            $scope = 'visit';
                            $aMatomoCustomVariables[] = "_paq.push(['setCustomVariable',{$variablesCount},'".CHtml::encode($label)."','".CHtml::encode($currentVariableValue)."','$scope']);";
                        }
                    }
                }
            }
        }
        if (empty($aMatomoCustomVariables)) {
            return;
        }
        $matomoCustomVariables = implode("\n", $aMatomoCustomVariables);
        App()->getClientScript()->registerScript('matomoCustomVariables', $matomoCustomVariables, CClientScript::POS_BEGIN);
    }

    /**
     * Add the setCustomDimension to js
     * @param integer $surveyId
     * @return $void
     */
    private function _setCustomDimension($surveyId)
    {
        $aMatomoCustomDimensions = array();
        $customDimensionNumber =  $this->get('customDimensionsNumber', null, null, 0);
        if ($customDimensionNumber > 0) {
            $aSurveyCustomDimensions = $this->get("customDimensions", 'survey', $surveyId);
            $aGlobalCustomDimensions = json_decode($this->get("customDimensions", null, null), 1);
            for ($dimensionsCount = 1; $dimensionsCount <= $customDimensionNumber; $dimensionsCount++) {
                $currentDimensionValue = !empty($aSurveyCustomDimensions[$dimensionsCount]) ? $aSurveyCustomDimensions[$dimensionsCount] : null;
                if (empty($currentDimensionValue)) {
                    $currentDimensionValue = !empty($aGlobalCustomDimensions[$dimensionsCount]) ? $aGlobalCustomDimensions[$dimensionsCount] : null;
                }
                if (!empty($currentDimensionValue)) {
                    $currentDimensionValue = $this->ProcessStepString($currentDimensionValue);
                    if ($currentDimensionValue) {
                        $aMatomoCustomDimensions[] = "_paq.push(['setCustomDimension',{$dimensionsCount},'".CHtml::encode($currentDimensionValue)."']);";
                    }
                }
            }
        }
        if (empty($aMatomoCustomDimensions)) {
            return;
        }
        $matomoCustomDimensions = implode("\n", $aMatomoCustomDimensions);
        App()->getClientScript()->registerScript('matomoCustomDimensions', $matomoCustomDimensions, CClientScript::POS_BEGIN);
    }

    /**
     * Add the setCustomDimension to js
     * @param integer $surveyId
     * @return $void
     */
    private function _setCustomUserId($surveyId)
    {
        $sCustomUserid = $this->_getCurrentSetting('matomoUserId', $surveyId);
        if (empty($sCustomUserid)) {
            return;
        }
        $sCustomUserid = $this->ProcessStepString($sCustomUserid);
        if (empty($sCustomUserid)) {
            return;
        }
        $oldMatomoPublicTrackCustomUserid = Yii::app()->session['matomoPublicTrackCustomUserid'];
        if ($oldMatomoPublicTrackCustomUserid && $oldMatomoPublicTrackCustomUserid != $sCustomUserid) {
            $this->setNewVisit = true;
        }
        Yii::app()->session['matomoPublicTrackCustomUserid'] = $sCustomUserid;
        $matomoCustomUserId = "_paq.push(['setUserId', '".CHtml::encode($sCustomUserid)."']);";
        App()->getClientScript()->registerScript('matomoCustomUserId', $matomoCustomUserId, CClientScript::POS_BEGIN);
    }

    /**
     * Add the param for ùmatomo plugin
     * @param integer $surveyId
     * @return $void
     */
    private function _setLimesurveyVisitPlugin($surveyId)
    {
        $blimesurveyVisitPlugin = $this->_getCurrentSetting('limesurveyVisitPlugin', $surveyId);
        if (empty($blimesurveyVisitPlugin)) {
            return;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (empty($oSurvey)) {
            return;
        }
        $oldSurveyId = Yii::app()->session['matomoPublicTrackSurveyId'];
        if ($oldSurveyId && $oldSurveyId != $surveyId) {
            $this->setNewVisit = true;
        }
        Yii::app()->session['matomoPublicTrackSurveyId'] = $surveyId;
        $aLimeSurveyVisitPlugin = array(
            'surveyid'=>$surveyId
        );
        $responseId = isset($_SESSION['survey_'.$surveyId]['srid']) ? intval($_SESSION['survey_'.$surveyId]['srid']) : null;
        if (!empty($responseId)) {
            $oldResponseId = Yii::app()->session['matomoPublicTrackResponseId'];
            if ($oldResponseId && $oldResponseId != $responseId) {
                $this->setNewVisit = true;
            }
            Yii::app()->session['matomoPublicTrackResponseId'] = $responseId;
            $aLimeSurveyVisitPlugin['srid'] = $responseId;
        }
        if ($oSurvey->anonymized != "Y" && $oSurvey->getHasTokensTable()) {
            $token = isset($_SESSION['survey_'.$surveyId]['token']) ? $_SESSION['survey_'.$surveyId]['token'] : null;
            if (!empty($token)) {
                $oldToken = Yii::app()->session['matomoPublicTrackToken'];
                if ($oldToken && $oldToken != $token) {
                    $this->setNewVisit = true;
                }
                Yii::app()->session['matomoPublicTrackToken'] = $token;
                $aLimeSurveyVisitPlugin['token'] = $token;
                $oToken = Token::model($surveyId)->findByToken($token);
                if ($oToken) {
                    $aLimeSurveyVisitPlugin['tokenid'] = $oToken->tid;
                    $description = trim($oToken->firstname." ".$oToken->lastname);
                    if (empty($description)) {
                        $description = $oToken->email;
                    } elseif (trim($oToken->email)) {
                        $description .= " (".$oToken->email.")";
                    }
                    if (!empty($description)) {
                        $aLimeSurveyVisitPlugin['tokeninfo'] = $description;
                    }
                }
            }
        }
        $matomoLimesurveyVisitPlugin = "_paq.push(['appendToTrackingUrl', 'limesurveyData=".json_encode($aLimeSurveyVisitPlugin)."']);\n";
        App()->getClientScript()->registerScript('matomoLimesurveyVisitPlugin', $matomoLimesurveyVisitPlugin, CClientScript::POS_BEGIN);
    }
    /**
     * Add the setCustomUrl to js
     * @param integer $surveyId
     * @param string[] $aUrlInfo forced params
     * @return $void
     */
    private function _setCustomURL($surveyId, $aUrlInfo = array())
    {
        $rewriteURLs =$this->_getCurrentSetting('rewriteURLs', $surveyId);
        if ($rewriteURLs) {
            //Write the custom URL to the piwikForLimeSurvey JS variable. This is then read inside loadPiwikTrackingCode()
            $this->aMatomoCustomUrlParams=array_replace($this->aMatomoCustomUrlParams, $aUrlInfo);
            switch ($rewriteURLs) {
                case 'admin':
                    $sCustomUrl=$this->_getAdminUrl();
                    break;
                case 'public':
                    $sCustomUrl=$this->_getPublicUrl();
                    break;
                case 'tweak':
                    $sCustomUrl=$this->_getTweakUrl();
                    break;
                default:
                    $sCustomUrl=null;
                    break;
            }
            if ($sCustomUrl) {
                $matomoCustomUrl="_paq.push(['setCustomUrl', '$sCustomUrl']);";
                App()->getClientScript()->registerScript('matomoCustomUrl', $matomoCustomUrl, CClientScript::POS_BEGIN);
            }
        }
    }

    /**
     * A function to rewrite to admin page with actual information
     * Return string
     */
    private function _getAdminUrl()
    {
        $aParams=$this->aMatomoCustomUrlParams;
        $sController='admin/survey/';
        switch ($aParams['base']) { // Todo : other controller
            case 'survey':
                $aParams['sa']='view';
                break;
            default:
                $aParams['sa']='index';
                $aParams['event']=($aParams['event']) ? $aParams['event'] : $aParams['base'];
                // leave base if other than survey listing
                break;
        }
        unset($aParams['base']);
        $sController.=Yii::app()->getUrlManager()->createPathInfo(array_filter($aParams), "/", "/");
        return Yii::app()->createUrl($sController);
    }

    /**
     * A function to rewrite to publi page with actual information
     * Return string
     */
    private function _getPublicUrl()
    {
        $aParams=$this->aMatomoCustomUrlParams;
        switch ($aParams['base']) { // Todo : other controller
            case 'survey':
                $sController='survey/index/';
                break;
            case 'statistics_user':
                $sController='statistics_user/view/'; // Not sure for this one
                break;
            default:
                $sController='surveys/index/';
                $aParams['event']=$aParams['base'];
                break;
        }
        $sController.=Yii::app()->getUrlManager()->createPathInfo(array_filter($aParams), "/", "/");
        return Yii::app()->createUrl($sController);
    }

    /**
     * A function to rewrite to a false url but with cleaner view in matomo
     * Return string
     */
    private function _getTweakUrl()
    {
        $aParams=$this->aMatomoCustomUrlParams;
        unset($aParams['base']);
        $sTweakedUrl = Yii::app()->getUrlManager()->createPathInfo(array_filter($aParams), "-", "/");
        $sTweakedUrl = str_replace("surveyid-", "", $sTweakedUrl);
        $sTweakedUrl = str_replace("lang-", "", $sTweakedUrl);
        return $sTweakedUrl;
    }

    /**
     * Get current setting for current survey (use empty string as null value)
     * @param string setting to get
     * @param integer survey id
     * @return string|array|null
     */
    private function _getCurrentSetting($setting, $surveyId = null)
    {
        if ($surveyId) {
            $value = $this->get($setting, 'Survey', $surveyId, '');
            if ($value !== '') {
                return $value;
            }
        }
        $default = (isset($this->settings[$setting]['default'])) ? $this->settings[$setting]['default'] : null;
        return $this->get($setting, null, null, $default);
    }

    /**
     * Set the setting for crossdomain
     * return @void
     */
    private function _setCrossDomainLinking()
    {
        $crossDomainLinkingSetting =$this->_getCurrentSetting('crossDomainLinking');
        if ($crossDomainLinkingSetting) {
            /* Fix each line */
            $aCrossDomains = $this->fixDomainList($crossDomainLinkingSetting,"*.");
            if ($aCrossDomains) {
                $CrossDomainLinking = "'".implode("','",$aCrossDomains)."'";
                $CrossDomainScript = "_paq.push(['setDomains', [{$CrossDomainLinking}]]);\n";
                $CrossDomainScript .= "_paq.push(['enableCrossDomainLinking']);\n";
                App()->getClientScript()->registerScript('matomoCrossDomainLinking', $CrossDomainScript, CClientScript::POS_BEGIN);
            }
        }
    }

    /**
     * Get a string multiple lines and return domain
     * @param $string
     * @param $replaceScheme : false|string : force replace of scheme part
     * @return array
     */
    private function fixDomainList($crossDomainLinkingSetting, $replaceScheme = false)
    {
        $aCrossDomainsLinking = preg_split('/\r\n|\r|\n/', $crossDomainLinkingSetting, -1, PREG_SPLIT_NO_EMPTY);
        $aCrossDomains = array();
        foreach($aCrossDomainsLinking as $crossDomainLinking) {
            $crossDomainLinking = trim($crossDomainLinking);
            $aUrl = parse_url($crossDomainLinking);
            if($aUrl && !empty($aUrl['host'])) {
                $domain = "";
                if(!$replaceScheme) {
                    $domain = $aUrl['scheme']."://".$aUrl['host'];
                } elseif(substr($aUrl['host'], 0, strlen($replaceScheme)) != $replaceScheme) {
                    $domain = $replaceScheme.$aUrl['host'];
                }
                if(!empty($aUrl['port'])) {
                    $domain .= ":".$aUrl['port'];
                }
                if(!empty($aUrl['path'])) {
                    $domain .= $aUrl['path'];
                }
                $aCrossDomains[] = $domain;
            }
        }
        return $aCrossDomains;
    }
    /**
     * Translation for plugin
     *
     * @param string $sToTranslate The message that are being translated
     * @param string $sEscapeMode
     * @param string $sLanguage
     * @return string
     */
    public function gT($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return parent::gT($sToTranslate, $sEscapeMode, $sLanguage);
    }

    /**
    * Get the complete HTML from a string with Expression for admin
    * @param string $sExpression : the string to parse
    * @param array $aReplacement : optionnal array of replacemement
    * @param boolean $forceEm : force EM or not
    *
    * @author Denis Chenu
    * @version 1.0
    */
    private function _getHtmlExpression($sExpression, $iSurveyId, $aReplacement = array(), $forceEm = false)
    {
        $LEM = LimeExpressionManager::singleton();
        $aReData=array();
        if ($iSurveyId) {
            $LEM::StartSurvey($iSurveyId, 'survey', array('hyperlinkSyntaxHighlighting'=>true));// replace QCODE
        }
        if ($forceEm) {
            $sExpression = "{".$sExpression."}";
        } else {
            $oFilter = new CHtmlPurifier();
            $sExpression =$oFilter->purify(viewHelper::filterScript($sExpression));
        }
        LimeExpressionManager::ProcessString($sExpression);
        //~ templatereplace($sExpression, $aReplacement,$aReData,__CLASS__,false,null,array(),true);
        return $LEM::GetLastPrettyPrintExpression();
    }

    /**
     * Get a expression string
     * @var $string
     * @var $string
     * @return $string
     */
    private function ProcessStepString($string)
    {
        $string = trim(LimeExpressionManager::ProcessStepString($string, array(), 3, true));// force static
        if (
            $string &&
            strpos($string, 'em-expression em-haveerror') &&
            Permission::model()->hasSurveyPermission(LimeExpressionManager::getLEMsurveyId(), 'content')
        ) {
            return '';
        }
        return $string;
    }
}
