<div class="row">
    <div class="col-lg-12 content-right">
    <?php
        echo CHtml::form($form['action']);
    ?>
    <h3 class="clearfix"><?php echo $lang['Matomo settings for this survey']; ?>
      <div class='pull-right'>
        <?php
          echo CHtml::htmlButton('<i class="fa fa-check" aria-hidden="true"></i> '.gT('Save'),array('type'=>'submit','name'=>'save'.$pluginClass,'value'=>'save','class'=>'btn btn-primary'));
          echo " ";
          echo CHtml::htmlButton('<i class="fa fa-check-circle-o " aria-hidden="true"></i> '.gT('Save and close'),array('type'=>'submit','name'=>'save'.$pluginClass,'value'=>'redirect','class'=>'btn btn-default'));
          echo " ";
          echo CHtml::link(gT('Close'),$form['close'],array('class'=>'btn btn-danger'));
        ?>
      </div>
    </h3>

    <?php foreach($aSettings as $legend=>$settings) {
      $this->widget('ext.SettingsWidget.SettingsWidget', array(
            //'id'=>'summary',
            'title'=>$legend,
            //~ 'prefix' => $pluginClass,
            'form' => false,
            'formHtmlOptions'=>array(
                'class'=>'form-core',
            ),
            'labelWidth'=>4,
            'controlWidth'=>8,
            'settings' => $settings,
        ));
    } ?>


    <?php echo CHtml::endForm();?>
    </div>
</div>
